
var $playListStack=[]
var genres = [ "rock",
               "ambient",
               "classical",
               "dance",
               "deep-house",
               "disco",
               "dubstep",
               "drum&bass",
               "electro",
               "electronic",
               "hardcore-techno",
               "hiphop", "house",
               "indie-rock",
               "jazz",
               "metal",
               "minimal-techno",
               "pop",
               "progressive-house",
               "punk",
               "r&b",
               "rap",
               "reggae",
               "soul",
               "tech-house",
               "trance",
               "trip-hop",
               "trap"
               ];



////////// Playlists

var $playlist = function(){
  $current_genre = genres[ getRandomInt(0, genres.length -1) ];
  $.get("http://api.soundcloud.com/tracks.json?client_id=144d1cf5ccc5033c8cd424312199a1fd&genre=" + $current_genre, function(data){
    $playlist = data
  });
}();

var $next_playlist = function(){
  $.get("http://api.soundcloud.com/tracks.json?client_id=144d1cf5ccc5033c8cd424312199a1fd&genre=" + genres[ getRandomInt(0, genres.length -1) ], function(data){
    $next_playlist = data
  });
}();

/////// Gets

function getNext() {
  // This is where we look up in the next track in the playlistObject.
  console.log('Finding next track.');
  index = getRandomInt(0, $playlist.length -1);
  var id = $playlist[index].id; // Remove this later

  if($playListStack.length < 5 ){
    $playListStack.unshift( id );
  }
  else{
    $playListStack.unshift( id );
    $playListStack.pop();
  }
  console.log( $playListStack );
  playTrack( id );
}

function getPrev() {
  // This is where we look up in the previous track in the playlistObject.
  console.log('Finding previous track.');
  if($playListStack.length>1){
    $playListStack.pop();
  }
  else{
  }
  console.log($playListStack);

  playTrack( $playListStack[$playListStack.length]);
}

function getRandom(){
  // Do some randomizing stuff here !!
  // And make sure we didn't just play it.

  // GET them tracks from soundcloud API.
  console.log("Here is the next playlist")
  console.log( $next_playlist )
}

/////// Unicorns

// Get ramdom integer in a range between min and max
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function playTrack(id) {
  console.log("Appending track.");
  var $track = $('<div class="track next ' + id +'">\
                    <iframe width="100%" height="100%" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/'+ id +'&amp;auto_play=false&amp;hide_related=true&amp;show_comments=false&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>\
                  </div>\
                    ');
  $( ".track_list.current" ).append($track);
}

//////// Gooooo's

var goRandom = function() {
  // This should get fired on a swipe left
  console.log("Going to a random playlist.");
  getRandom();

  $('.container').append( $('<div class="track_list next"></div>') )
  $('.track_list.current').removeClass('current').addClass('prev');
  $('.track_list.next').removeClass('next').addClass('current');
  goNext();
}

var goNext = function() {
  // This should get fired on a swipe down
  console.log('Going to next track.');
  getNext();
  $('.track.current').addClass('prev').removeClass('current');
  $('.track.next').addClass('current').removeClass('next');
}

var goPrev = function() {
  // This should fire on swipe up and go to the previously played track.
  console.log('Going to previous track.');
  getPrev();
  $('.track.current').addClass('next').removeClass('current');
  $('.track.prev').addClass('current').removeClass('prev');
}



///////////////////////// Fire

$(document).ready(function(){


  // Swipe Events
  document.body.addEventListener('swd', goPrev, false);
  document.body.addEventListener('swu', goNext, false);
  document.body.addEventListener('swl', goRandom, false);

  // Keyboard events

  key('up', goPrev);
  key('down', goNext);
  key('right', goRandom);


  // buttons

  $('.menu_toggle').click(function(){
    $( this ).toggleClass('active');
    $('.menu').toggleClass('active');
    $('.container').toggleClass('inactive');
  });

  $('.swipe_down').click(function(){
    console.log("clicked");
    goPrev();
  });

  $('.swipe_up').click(function(){
    console.log("clicked");
    goNext();
  });

  $('.swipe_left').click(function(){
    console.log("clicked");
    goRandom();
  });


})
